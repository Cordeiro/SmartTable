package com.example.mateu.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import layout.SeconAct.ActLoginUser;

public class MainActivity extends AppCompatActivity {
    private static int temp_splash = 5000; // 5 segundos
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        new Handler().postDelayed(new Runnable(){
            // CARREGAR IMAGEM
            @Override
            public void run() {
                /*ESTE METODO SERA EXECUTADO POR 5 SEGUNDOS
                ANTES DE INICIAR A MAINACTIVITY*/
                Intent intent = new Intent(MainActivity.this, ActLoginUser.class);
                startActivity(intent);
                finish();
            }
        },temp_splash);
    }
    public void onClick(View view){
        Intent it = new Intent(this,ActLoginUser.class);
        startActivity(it);
    }

    /*public void cliqueLogin (View view){
        EditText editText = (EditText) findViewById(R.id.editText2);
        TextView textView2 = (TextView) findViewById(R.id.textView2);
        textView2.setText("Bem Vindo Funcionário "+editText.getText());
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
