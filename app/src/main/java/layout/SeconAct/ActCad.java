package layout.SeconAct;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.mateu.myapplication.R;

public class ActCad extends AppCompatActivity implements View.OnClickListener{

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_cad);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // CRIANDO E PREENCHENDO UM (SPINNER) COMBOBOX
        String[] Estados = new String[]{"AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MT","MS"
                ,"MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SP","SE","TO"};
        Spinner combo = (Spinner) findViewById(R.id.spinnerEstado);
        ArrayAdapter adp = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Estados);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_item);
        combo.setAdapter(adp);

        Intent intent = getIntent();
        if (intent != null){ // VERIFICA SE RECEBEU ALGUMA INTENT
            Bundle bundle = intent.getExtras();
            if (bundle != null){ // VERIFICA SE BUNDLE TEM ALGUMA CHAVE
                String nome = bundle.getString("Nome"); //RECEBE DA BUNDLE
                EditText editText = (EditText) findViewById(R.id.editCliNome); //ACESSA EDITTEXT
                editText.setText(nome); //SETA NOME NO EDITTEXT
            }
        }
    }

    public void onClick(View view){
        finish();
    }
}
