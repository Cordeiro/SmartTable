package layout.SeconAct;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mateu.myapplication.R;

public class ActLoginUser extends Activity {
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login_user);
        overridePendingTransition(R.anim.act_login_user_entrando,R.anim.act_main_saindo); //EFEITO ANIM
        button = (Button) findViewById(R.id.buttonLogCad);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editText= (EditText) findViewById(R.id.editLogNome);

                Bundle bundle = new Bundle();
                //SEGUE UM EX DE UM MAP(CHAVE,VALOR)
                bundle.putString("Nome",editText.getText().toString());

                Intent it = new Intent(getBaseContext(), ActCad.class);
                it.putExtras(bundle);
                startActivity(it);
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        switch (id){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        super.finish();

        overridePendingTransition(R.anim.act_main_entrando,R.anim.act_login_user_saindo);
    }
}